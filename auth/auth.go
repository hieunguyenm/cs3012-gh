package auth

import (
	"bufio"
	"context"
	"os"

	"github.com/google/go-github/github"
	"github.com/pkg/errors"
	"golang.org/x/oauth2"
)

const keyFile = "./apikey.txt"

// NewGitHubClient creates a GitHub API client.
func NewGitHubClient(ctx context.Context) (*github.Client, error) {
	key, err := readAPIKey()
	if err != nil {
		return nil, errors.Wrap(err, "failed to read API key")
	}
	token := oauth2.StaticTokenSource(&oauth2.Token{AccessToken: key})
	return github.NewClient(oauth2.NewClient(ctx, token)), nil
}

func readAPIKey() (string, error) {
	f, err := os.Open(keyFile)
	if err != nil {
		return "", errors.Wrap(err, "failed to open API key file")
	}
	defer f.Close()
	key, _, err := bufio.NewReader(f).ReadLine()
	if err != nil {
		return "", errors.Wrap(err, "failed to read string from API key file")
	}
	return string(key), nil
}
