# cs3012-gh

GitHub visualisation for CS3012. Shows the commit history of users who made commits in the last 12 months. 

[**Click here**](http://hieu.netsoc.ie/lmt1.mp4) for a video of how the visualisation works.

## Running the project

1. Put your GitHub **personal access token** inside a file called `apikey.txt` in the top-level directory of this project.
2. Run `go run . -port <port number>` in the top-level directory. The default port is 80.
    * The default port may not work on some systems. It is preferable to use a port number greater than 1024.
3. Navigate to `http://localhost/<owner>/<repository>` to view the commit history of that repository for the past 12 months.

## How it works

The project will use the personal access token to access the Github API. Since calculating the commit history for a repository can take a long time, the project will cache the results for the day of the calculation. All queries made on that day will be fetched from cache. 

Given the owner and repository specified in the URL, it will first check its database to see if the data has been cached. If it is, the result will be fetched from cache. If not, it queries GitHub for all the commits pushed for each month within the past 12 months. It then translates the data into JSON format which will be templated into the HTML page that will be served by the Go template engine. The webserver will then serve the templated HTML.

On page load, D3 will parse the templated JSON data and create a graph of the data.