package controller

import "time"

type commit struct {
	User  string  `json:"user"`
	Count [12]int `json:"count"`
}

type commitInfo struct {
	DateString []string `json:"dateString"`
	Data       []commit `json:"data"`
}

type databaseKey struct {
	Owner      string    `json:"owner"`
	Repository string    `json:"repository"`
	Date       time.Time `json:"date"`
}
