package controller

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/dgraph-io/badger"
	"github.com/gin-gonic/gin"
	"github.com/google/go-github/github"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

var (
	log *logrus.Logger
	g   *github.Client
	db  *badger.DB
	ctx = context.Background()
)

// InitController initialises the logger and GitHub client.
func InitController(c *github.Client, l *logrus.Logger, d *badger.DB) {
	g = c
	log = l
	db = d
}

// ListCommitHistory computes the list of commits of a repository.
func ListCommitHistory(c *gin.Context) {
	owner := c.Params.ByName("user")
	repo := c.Params.ByName("repo")
	if owner == "" || repo == "" {
		respondJSONError(c, errors.New("owner/repo is empty"), "URL should be in the form /history/<owner>/<repo>")
		return
	}
	t := time.Now()
	now := time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, time.UTC)
	d := databaseKey{owner, repo, now}
	p, err := getFromDatastore(d)
	if err == nil {
		c.HTML(http.StatusOK, "index.html", gin.H{
			"jsonData": string(p),
			"user":     owner,
			"repo":     repo,
			"from":     t.AddDate(0, -11, 0).Format("2006-01-02 15:04:05"),
			"to":       t.Format("2006-01-02 15:04:05"),
		})
		return
	}
	x, err := processCommitHistory(owner, repo, now)
	if err != nil {
		respondJSONError(c, err, "failed to process commit history")
		return
	}
	j, _ := json.Marshal(x)
	c.HTML(http.StatusOK, "index.html", gin.H{
		"jsonData": string(j),
		"user":     owner,
		"repo":     repo,
		"from":     t.AddDate(0, -11, 0).Format("2006-01-02 15:04:05"),
		"to":       t.Format("2006-01-02 15:04:05"),
	})
}

func processCommitHistory(owner, repo string, now time.Time) (*commitInfo, error) {
	d := map[string][12]int{}
	firstDay := time.Date(now.Year(), now.Month(), 1, 0, 0, 0, 0, time.UTC)
	var dateString []string
	for i := 0; i < 12; i++ {
		c, err := fetchCommitsUntil(owner, repo, now.AddDate(0, -i, 0))
		if err != nil {
			return nil, errors.Wrap(err, fmt.Sprintf("failed to get commit history for %v", now.AddDate(0, -i, 0)))
		}
		for _, v := range c {
			n := v.GetCommitter().GetLogin()
			a := d[n]
			a[i]++
			d[n] = a
		}
		month := firstDay.AddDate(0, -11+i, 0)
		dateString = append(dateString, month.Format("Jan 06"))
	}
	var processed []commit
	for k, v := range d {
		processed = append(processed, commit{k, v})
	}
	x := &commitInfo{dateString, processed}
	insertKeyValue(databaseKey{owner, repo, now}, x)
	return x, nil
}

func fetchCommitsUntil(owner, repo string, until time.Time) ([]*github.RepositoryCommit, error) {
	y := until.AddDate(-1, 0, 0)
	opt := &github.CommitsListOptions{
		ListOptions: github.ListOptions{PerPage: 100},
		Since:       time.Date(y.Year(), y.Month(), y.Day(), 0, 0, 0, 0, time.UTC),
		Until:       until,
	}
	var history []*github.RepositoryCommit
	for {
		h, res, err := g.Repositories.ListCommits(ctx, owner, repo, opt)
		if err != nil {
			return nil, errors.Wrap(err, "failed to get list paginated commit")
		}
		history = append(history, h...)
		if res.NextPage == 0 {
			break
		}
		opt.Page = res.NextPage
	}
	return history, nil
}

func respondJSONError(c *gin.Context, err error, msg string) {
	log.WithField("err", err).Error(msg)
	c.JSON(http.StatusBadRequest, gin.H{
		"error": fmt.Sprintf(msg)},
	)
}
