package controller

import (
	"encoding/json"

	"github.com/dgraph-io/badger"
	"github.com/pkg/errors"
)

// InitStore initialises the key-value store.
func InitStore(path string) (*badger.DB, error) {
	opts := badger.DefaultOptions
	opts.Dir = path
	opts.ValueDir = path
	return badger.Open(opts)
}

func insertKeyValue(key databaseKey, value *commitInfo) error {
	k, err := json.Marshal(key)
	if err != nil {
		return errors.Wrap(err, "failed to marshal key")
	}
	v, err := json.Marshal(value)
	if err != nil {
		return errors.Wrap(err, "failed to marshal value")
	}
	return db.Update(func(txn *badger.Txn) error {
		err := txn.Set(k, v)
		return errors.Wrap(err, "failed to update datastore")
	})
}

func getFromDatastore(key databaseKey) ([]byte, error) {
	k, err := json.Marshal(key)
	if err != nil {
		return nil, errors.Wrap(err, "failed to marshal key")
	}
	var c []byte
	if err = db.View(func(txn *badger.Txn) error {
		item, err := txn.Get(k)
		if err != nil {
			return errors.Wrap(err, "failed to get value")
		}
		c, err = item.ValueCopy(nil)
		if err != nil {
			return errors.Wrap(err, "failed to copy value")
		}
		return nil
	}); err != nil {
		return nil, errors.Wrap(err, "failed to create datastore view")
	}
	return c, nil
}
