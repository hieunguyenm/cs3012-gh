package routing

import (
	"cs3012-gh/controller"

	"github.com/gin-gonic/gin"
	"github.com/google/go-github/github"
	"github.com/sirupsen/logrus"
)

// NewRouter takes a GitHub client and logger and returns a new router engine
// with templates and routes loaded.
func NewRouter(c *github.Client, l *logrus.Logger) *gin.Engine {
	gin.SetMode(gin.ReleaseMode)
	r := gin.Default()
	r.LoadHTMLFiles("./static/index.html")
	assignRoutes(r)
	return r
}

func assignRoutes(r *gin.Engine) {
	r.GET("/:user/:repo", controller.ListCommitHistory)
}
