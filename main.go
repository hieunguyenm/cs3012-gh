package main

import (
	"context"
	"flag"
	"fmt"

	"cs3012-gh/auth"
	"cs3012-gh/controller"
	"cs3012-gh/logging"
	"cs3012-gh/routing"
)

var (
	port    = flag.Int("port", 80, "port to run server")
	storage = flag.String("db", "./storage", "database file for storage")
)

func main() {
	flag.Parse()
	log := logging.NewLogger()
	ctx := context.Background()

	client, err := auth.NewGitHubClient(ctx)
	if err != nil {
		log.WithField("err", err).Fatal("Failed to create GitHub client, do you have your API key in ./apikey.txt?")
	}

	db, err := controller.InitStore(*storage)
	if err != nil {
		log.WithField("err", err).Fatal("Failed to initialise database storage")
	}
	controller.InitController(client, log, db)
	router := routing.NewRouter(client, log)
	log.WithField("port", *port).Info("Server running")
	router.Run(fmt.Sprintf(":%d", *port))
}
